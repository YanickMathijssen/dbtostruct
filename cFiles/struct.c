#include <stdio.h>
#include <string.h>
#define MAXCHAR 1000

struct scores{

    char name[256];
    char lastname[256];
    char country[256];
    int math;
    int physics;

};

int main(){

    FILE *fp;
    char str[MAXCHAR];
    char *filename = "../db/base.txt";

    fp = fopen(filename, "r");
    if (fp == NULL){
        printf("could not open the file %s", filename);
        return 1;
    }

    while (fgets(str, MAXCHAR, fp) != NULL){
        int init_size = strlen(str);
        char delim[] = " ";
        char *ptr = strtok(str, delim);
        int i = 0;

        while(ptr != NULL)
        {
           
            printf("%s\n", ptr);
            ptr = strtok(NULL, delim);
                       
        }
    }
        fclose(fp);
        return 0;

}